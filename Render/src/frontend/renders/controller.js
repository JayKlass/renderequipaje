/*!
governify-render 1.0.0, built on: 2018-05-09
Copyright (C) 2018 ISA group
http://www.isa.us.es/
https://github.com/isa-group/governify-render#readme

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


$scope.date = new Date().toISOString();
$scope.maletaGrande = {};
$scope.maletaPequenia = {};
$scope.neceser = {};
$scope.limiteNeceser = 4;
$scope.limitePeque = 8;
$scope.limiteGrande = 16;

$scope.anadirNeceser = function anadirNeceser(equipaje) {
    
    if (!$scope.neceser[equipaje.titulo] && $scope.limiteNeceser - equipaje.espacio >= 0) {
        $scope.neceser[equipaje.titulo] = equipaje
        $scope.neceser[equipaje.titulo].cantidad = 0
        $scope.espacioDisponibleNeceser = 0;
    }else{
        $scope.neceser[equipaje.titulo].message = "El neceser esta lleno, no se ha podido añadir " + equipaje.titulo;
    }
    if($scope.espacioDisponibleNeceser < 4 && $scope.limiteNeceser - equipaje.espacio >= 0){
        $scope.neceser[equipaje.titulo].cantidad += 1;
        $scope.espacioDisponibleNeceser += equipaje.espacio;
        $scope.limiteNeceser -= equipaje.espacio;
    }else{
        $scope.neceser[equipaje.titulo].message = "El neceser esta lleno, no se ha podido añadir " + equipaje.titulo;
    }
}


$scope.anadirMaletaPequenia = function anadirMaletaPequenia(equipaje) {
    
    if (!$scope.maletaPequenia[equipaje.titulo] && $scope.limitePeque - equipaje.espacio >= 0) {
        $scope.maletaPequenia[equipaje.titulo] = equipaje
        $scope.maletaPequenia[equipaje.titulo].cantidadPeque = 0
        $scope.espacioDisponiblePeque = 0;
    }else{
        $scope.maletaPequenia[equipaje.titulo].messagePeque = "La maleta pequeña esta llena, no se ha podido añadir " + equipaje.titulo;
    }
    if($scope.espacioDisponiblePeque < 8 && $scope.limitePeque - equipaje.espacio >= 0){
        $scope.maletaPequenia[equipaje.titulo].cantidadPeque += 1;
        $scope.espacioDisponiblePeque += equipaje.espacio;
        $scope.limitePeque -= equipaje.espacio;
    }else{
        $scope.maletaPequenia[equipaje.titulo].messagePeque = "La maleta pequeña esta llena, no se ha podido añadir " + equipaje.titulo;
    }
}


$scope.anadirMaletaGrande = function anadirMaletaGrande(equipaje) {
   
    if (!$scope.maletaGrande[equipaje.titulo] && $scope.limiteGrande - equipaje.espacio >= 0) {
        $scope.maletaGrande[equipaje.titulo] = equipaje
        $scope.maletaGrande[equipaje.titulo].cantidadGrand = 0
        $scope.espacioDisponibleGrande = 0;
    }else{
        $scope.maletaGrande[equipaje.titulo].messageGrande = "La maleta grande esta llena, no se ha podido añadir " + equipaje.titulo;
    }
    if($scope.espacioDisponibleGrande < 16 && $scope.limiteGrande - equipaje.espacio >= 0){
        $scope.maletaGrande[equipaje.titulo].cantidadGrand += 1;
        $scope.espacioDisponibleGrande += equipaje.espacio;
        $scope.limiteGrande -= equipaje.espacio;
    }else{
        $scope.maletaGrande[equipaje.titulo].messageGrande = "La maleta grande esta llena, no se ha podido añadir " + equipaje.titulo;
    }
}
